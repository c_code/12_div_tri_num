/*
 * Divisible Triangle Numbers 
 * 
 *       by Jeremy Tobac 
 *             2014
 *
 * Find the first triangle number to have over 500 divisors 
 * triange number: number made up of all before it so the 2nd triangle number is three: 1 + 2 = 3
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**@brief Finds the next triangle number and its position
 *
 *@param pintTrim: The current triangle number 
 *@param pintPos : Position of the current triangle number
 *
 */
void intNxtTriNum(int *pintTriNum, int* pintPos)
{
	*pintTriNum+=++(*pintPos);
}

/**@brief Finds how many numbers the param is divisible by
 *
 *To shorten the search of divisible numbers, 
 *this function only searches up to the square root of the number being tested
 *and counts each number as 2 divisors, 
 *unless the divisor is the square root of the number being tested
 *
 *@param int intNum: Number to find amount of divisors of
 *
 *ret Returns the number of divisors of intNum from [1, intNum]
 */
int nDivrs(int intNum)
{
	int intNumDvrs = 0;
	int intTest = 1;
	double dblSqrRt = pow((double)intNum, 0.5);
	

	while(intTest <= dblSqrRt){
		if(intNum % intTest == 0){
			intNumDvrs += 2;
		}
		intTest++;
	}

	intTest--;

	if((double)intTest == dblSqrRt){
		intNumDvrs--;
	}

	return intNumDvrs;
}

int main(int argc, char* argv[])
{
	int intTriNum = 0;
	int intPos = 0;
	int intNumDvrs = 0;

	while(intNumDvrs < 500){
		intNxtTriNum(&intTriNum, &intPos);
		intNumDvrs = nDivrs(intTriNum);

		printf("Triangle number %d: equals %d: with %d divisors\n", intPos, intTriNum, intNumDvrs);
	}

	return 0;
}
